<table>
    <thead>
        <tr>
            <th rowspan="4" colspan="2" style="background: white;"></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Razón Social: Staff Boom Perú S.A.C.</td>
        </tr>
        <tr>
            <td>RUC: 20605944681</td>
        </tr>
        <tr>
            <td>Período: {{ $from }}  to  {{ $to }}</td>
        </tr>

        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>

    </tbody>
</table>

<table>
    <thead>
        <tr>
            {{-- <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">ID</th> --}}
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Cuenta</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Nombre Completo</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">DNI/C.E</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Jornada Laboral</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Fecha</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Entrada</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Salida R1</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Entrada R1</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Salida Refrigerio</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Entrada Refrigerio</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Salida R2</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Entrada R2</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Salida</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Tiempo Efectivo Trabajado
            </th>
            @if($showReason == "default")
                <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Razones de Edición</th>
            @endif

        </tr>
    </thead>
    <tbody>
        @foreach ($markings as $marking)
        <tr>
            {{-- <td style="text-align:center;">{{ $marking->id }}</td> --}}
            <td style="text-align:center;">{{ $marking->ACCOUNT }}</td>
            <td style="text-align:center;">{{ $marking->NOMBRE_COMPLETO }}</td>
            <td style="text-align:center;">{{ $marking->DOCUMENTO }}</td>
            <td style="text-align:center;">{{ $marking->JORNADA }}</td>
            <td style="text-align:center;">{{ $marking->FECHA }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_1 == 1) ? 'color:red;' : ''}} ">{{ $marking->in_1 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_1 == 1) ? 'color:red;' : ''}} ">{{ $marking->out_1 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_2 == 1) ? 'color:red;' : ''}} ">{{ $marking->in_2 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_2 == 1) ? 'color:red;' : ''}} ">{{ $marking->out_2 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_3 == 1) ? 'color:red;' : ''}} ">{{ $marking->in_3 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_3 == 1) ? 'color:red;' : ''}} ">{{ $marking->out_3 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_4 == 1) ? 'color:red;' : ''}} ">{{ $marking->in_4 }}</td>
            <td style="text-align:center; {{ ($showReason == "default" && $marking->update_4 == 1) ? 'color:red;' : ''}} ">{{ $marking->out_4 }}</td>
            <td style="text-align:center;">{{ $marking->TotalTime }}</td>
            @if($showReason == "default")
                <td style="text-align:center;">{{ $marking->REASON }}</td>
            @endif
        </tr>
        @endforeach



    </tbody>
</table>