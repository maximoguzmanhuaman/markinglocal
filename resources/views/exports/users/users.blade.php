<table>
    <thead>
        <tr>
            <th rowspan="4" colspan="3" style="background: white;"></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td></td>

        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Name</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Dni</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Account</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Job Title</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Date Hired</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Time of Entry</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Time of Out</th>
            <th style="background: #0071C1;color:white; font-weight: bold; text-align:center;">Status</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->name ?? '' }}</td>
                <td>{{ $user->dni ?? '' }}</td>
                <td>{{ $user->account ? $user->account->name : ''  }}</td>
                <td>{{ $user->jobtitle ? $user->jobtitle->name : ''  }}</td>
                <td>{{ $user->date_hired ?? ''}}</td>
                <td>{{ $user->time_of_entry ? date('H:i', strtotime($user->time_of_entry)) : '' }}</td>
                <td>{{ $user->time_of_out ? date('H:i', strtotime($user->time_of_out)) : '' }}</td>
                <td>{{ $user->status ? 'Active' : 'Inactive' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
