<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\DocumentTypeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\JobTitleController;
use App\Http\Controllers\MarkingController;
use App\Http\Controllers\ParentTitleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Resources\UserResourceEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    return new UserResourceEdit($request->user());
});

// Route::apiResource('roles', RoleController::class);

// Route::apiResource('permissions', PermissionController::class);
// Route::apiResource('users', UserController::class);
// Route::apiResource('accounts', AccountController::class);
// Route::apiResource('jobTitles', JobTitleController::class);
// Route::apiResource('documentTypes', DocumentTypeController::class);
// Route::apiResource('contracts', ContractController::class);


// // Exports
// Route::get('export/users', [UserController::class, 'exportUsers']);
// Route::get('export/markings', [MarkingController::class, 'exportMarkings']);



// // Route::get('aaaaaaaa', [MarkingController::class, 'testing']);

// //Markings
// Route::apiResource('markings', MarkingController::class);
// Route::post('add-marking-from-admin', [MarkingController::class, 'storeFromAdmin']);

// Route::get('get-last-marking/user/{id}', [MarkingController::class, 'getLastMarking']);
// Route::get('get-markings-day/user/{id}', [MarkingController::class, 'getMarkingByUser']);


Route::middleware(['auth:sanctum',])->group(function () {

    Route::apiResource('parent_titles', ParentTitleController::class)->middleware('auth:sanctum');
    Route::apiResource('roles', RoleController::class);
    Route::apiResource('permissions', PermissionController::class);
    Route::apiResource('users', UserController::class);
    Route::apiResource('accounts', AccountController::class);
    Route::apiResource('jobTitles', JobTitleController::class);
    Route::apiResource('documentTypes', DocumentTypeController::class);
    Route::apiResource('contracts', ContractController::class);

    // Exports
    Route::get('export/users', [UserController::class, 'exportUsers']);
    Route::get('export/markings', [MarkingController::class, 'exportMarkings']);

    //Markings
    Route::apiResource('markings', MarkingController::class);
    Route::post('add-marking-from-admin', [MarkingController::class, 'storeFromAdmin']);
    Route::get('get-last-marking/user/{id}', [MarkingController::class, 'getLastMarking']);
    Route::get('get-markings-day/user/{id}', [MarkingController::class, 'getMarkingByUser']);

    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('abilities', function (Request $request) {
        return $request->user()->roles()->with('permissions')
            ->get()
            ->pluck('permissions')
            ->flatten()
            ->pluck('name')
            ->unique()
            ->values()
            ->toArray();
    });
});




Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
