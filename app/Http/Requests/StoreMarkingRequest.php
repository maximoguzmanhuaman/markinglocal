<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMarkingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'entry_1' => 'nullable',
            'out_1' => 'nullable',
            'entry_2' => 'nullable',
            'out_2' => 'nullable',
            'entry_3' => 'nullable',
            'out_3' => 'nullable',
            'entry_4' => 'nullable',
            'out_4' => 'nullable',
        ];
    }
}
