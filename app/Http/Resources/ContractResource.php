<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            'user_id' => $this->user->id ?? null,
            'user' => [
                "id" => $this->user->id ?? null,
                "name" => $this->user->name ?? null
            ],
            'account_id' => $this->account->id ?? null,
            'account' => [
                "id" => $this->account->id ?? null,
                "name" => $this->account->name ?? null
            ],
            'job_title_id' => $this->jobtitle->id ?? null,
            'jobtitle' => [
                "id" => $this->jobtitle->id ?? null,
                "name" => $this->jobtitle->name ?? null
            ],
            'time_of_entry' => $this->time_of_entry->format('H:i'),
            'time_of_out' => $this->time_of_out->format('H:i'),
            'contract_date_start' => $this->contract_date_start,
            'contract_date_end' => $this->contract_date_end,
            'status_string' => $this->status ? 'Active' : 'Inactive',
            'status' => $this->status

        ];
    }
}
