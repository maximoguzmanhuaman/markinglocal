<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarkingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => [
                "id" => $this->user->id ?? null,
                "name" => $this->user->name ?? null,
                'dni' => $this->user->dni ?? null,
                'time_of_entry' =>  $this->user->time_of_entry ? date('h:i A', strtotime($this->user->time_of_entry)) : "", //$this->user->time_of_entry ?? null,
                'time_of_out' => $this->user->time_of_out ? date('h:i A', strtotime($this->user->time_of_out)) : "", // $this->user->time_of_out ?? null,
            ],
            'account' => [
                "id" => $this->user->account->id ?? null,
                "name" => $this->user->account->name ?? null,
            ],
            'in' => $this->clock_in ? $this->clock_in->format('H:i') : '',//\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$this->clock_in)->format('h:i'),
            'out' => $this->clock_out ? $this->clock_out->format('H:i') : '',
            'date' => $this->date,
            'status'=> $this->status,
            'reason' => $this->reason ?? '',
        ];

        // return [
        //     'id' => $this->id,
        //     'entry' => $this->entry,
        //     // 'user_id' => $this->user_id,
        //     // 'user_name' => $this->user->name ?? null,
        //     // 'user_dni' => $this->user->dni ?? null,
        //     // 'user_time_of_entry' =>  $this->user->time_of_entry ? date('h:i A', strtotime($this->user->time_of_entry)) : "", //$this->user->time_of_entry ?? null,
        //     // 'user_time_of_out' => $this->user->time_of_out ? date('h:i A', strtotime($this->user->time_of_out)) : "", // $this->user->time_of_out ?? null,
        //     // 'account_id' => $this->user->account->id ?? null,
        //     // 'account_name' => $this->user->account->name ?? null,

        //     'user' => $this->user,
        //     'account' => $this->user->account

        //     // 'account' => [
        //     //     "id" => $this->user->account->id ?? null,
        //     //     "name" => $this->user->account->name ?? null,
        //     // ]
        // ];
    }
}
