<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResourceEdit extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            'dni' => $this->dni,
            'account_id' => $this->account->id ?? null,
            'account' => [
                "id" => $this->account->id ?? null,
                "name" => $this->account->name ?? null
            ],
            'document_type_id' => $this->documenttype->id ?? null,
            'documenttype' => [
                "id" => $this->documenttype->id ?? null,
                "name" => $this->documenttype->name ?? null
            ],
            'job_title_id' => $this->jobtitle->id ?? null,
            'jobtitle' => [
                "id" => $this->jobtitle->id ?? null,
                "name" => $this->jobtitle->name ?? null
            ],
            'role' => [
                "id" => $this->roles[0]->id ?? null,
                "name" => $this->roles[0]->name ?? null,
            ],
            'status' => $this->status,
            // 'summer_schedule' => $this->summer_schedule,
            'date_hired' => $this->date_hired,
            'time_of_entry' => $this->time_of_entry->format('H:i'),
            'time_of_out' => $this->time_of_out->format('H:i'),
            // 'email' => $this->email,
            'contract_id' => $this->contract->id ?? null,
            'contract' =>  new ContractResource($this->contract ?? null)
        ];
    }
}
