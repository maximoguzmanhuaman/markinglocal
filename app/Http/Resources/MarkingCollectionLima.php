<?php

namespace App\Http\Resources;
use Carbon\Carbon;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MarkingCollectionLima extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        dd("entro");
        $return = array();
       // dd($this->collection);
        foreach($this->collection as $element){
            dd($element);
            $return[] = [
                'ACCOUNT' => $element->ACCOUNT,
                'NOMBRE_COMPLETO' => $element->NOMBRE_COMPLETO,
                'DOCUMENTO' => $element->DOCUMENTO,
                'JORNADA' => $element->JORNADA,
                'USER_ID' => $element->USER_ID,
                'entrada' => Carbon::parse($element->in_1,'America/Los_Angeles')
                ->timezone('America/Lima') 
                ->format('H:m'),
                'in_2' => Carbon::parse($element->in_2,'America/Los_Angeles')
                ->timezone('America/Lima') 
                ->format('H:m'),
                'in_3' => Carbon::parse($element->in_3,'America/Los_Angeles')
                ->timezone('America/Lima') 
                ->format('H:m'),
                'in_4' => Carbon::parse($element->in_4,'America/Los_Angeles')
                ->timezone('America/Lima') 
                ->format('H:m'),
                
            ];
        }
        return $return;

    }
}
