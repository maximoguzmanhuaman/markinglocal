<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // $request->validate([
        //     'name' => ['required'],
        //     'email' => ['required', 'email', 'unique:users'],
        //     'password' => ['required', 'min:8', 'confirmed'],
        //     'password_confirmation' => ['required'],
        // ]);

        // User::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password),

        // ]);

        return response()->json([
            'msg' => 'Register Success!'
        ]);
    }

    public function login(Request $request)
    {

        // dd($request);

        $request->validate([
            // 'email' => 'required|email',
            'dni' => 'required|string|min:8|max:9',
            'password' => 'required',
            'device_name' => 'required',
        ]);

        $user = User::where('dni', $request->dni)->where('status',1)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'dni' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $user->createToken($request->device_name)->plainTextToken;
    }
    public function logout(Request $request)
    {
        // Revoke the token that was used to authenticate the current request...
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'msg' => 'Logout Success!'
        ]);
    }
}
