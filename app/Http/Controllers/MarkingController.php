<?php

namespace App\Http\Controllers;

use App\Exports\MarkingsExport;
use App\Models\Marking;
use App\Models\User;
use App\Http\Requests\StoreMarkingRequest;
use App\Http\Requests\UpdateMarkingRequest;
use App\Http\Resources\MarkingResource;
use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class MarkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $data = Marking::select(
        //     'markings.id',
        //     'markings.date',
        //     DB::raw("(DATE_FORMAT(markings.clock_in,'%H:%i')) as clk_in"),
        //     DB::raw("(DATE_FORMAT(markings.clock_out,'%H:%i')) as clk_out"),
        //     'users.name',
        //     'users.dni'
        // )
        //     ->leftjoin('users', 'users.id', '=', 'markings.user_id')
        //     ->wherein('markings.status', [1, 2])
        //     ->orderby('markings.id', 'desc')
        //     ->get();
        // ->paginate(10);

        $twoMonthsAgo = now()->subMonths(2)->format('Y-m-d');

        $data = Marking::select(
                'markings.id',
                'markings.date',
                DB::raw("(DATE_FORMAT(markings.clock_in,'%H:%i')) as clk_in"),
                DB::raw("(DATE_FORMAT(markings.clock_out,'%H:%i')) as clk_out"),
                'users.name',
                'users.dni'
            )
            ->leftJoin('users', 'users.id', '=', 'markings.user_id')
            ->whereIn('markings.status', [1, 2])
            ->whereDate('markings.date', '>=', $twoMonthsAgo)
            ->orderBy('markings.id', 'desc')
            ->get();


        return  $data;
    }



    public function testing()
    {
        $markings = Marking::with(['user' => function ($query) {
            $query->with('account');
        },])
            ->whereDate('created_at', '2022-10-19')
            ->get();



        return User::with(['jobtitle', 'account'])->get();


        //return $newMarkings;

        // $array = Marking::all()->groupBy('user_id')->toArray();

        // return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMarkingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $agent = new Agent();

        if (!$agent->isWindows()) {
            return response()->json([
                "status" => 400,
                'message' => 'This action cannot be performed from a Mobile device'
            ], 400);
        }


        // //Validate if is a Mobile
        // $isMobile = $this->is_mobile();

        // if($isMobile){
        //     return response()->json([
        //         "status" => 400,
        //         'message' => 'This action cannot be performed from a Mobile device'
        //     ], 400);

        // }

        $date = Carbon::now()->timezone('America/Los_Angeles')->isoFormat('YYYY-MM-DD');
        $dateMarking = Carbon::now()->timezone('America/Los_Angeles')->isoFormat('YYYY-MM-DD H:m:s');


        $contract = Contract::where('user_id', $request->user_id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->first();

        if ($request->step == 1) {


            $verifyStatus = $this->getLastMarking($request->user_id);

            if (!$verifyStatus) {

                $marking = Marking::create([
                    'user_id'     => $request->user_id,
                    'clock_in'    => $dateMarking,
                    'contract_id' => $contract->id,
                    'date'    => $date,
                ]);
                try {
                    //code...
                    // event(new \App\Events\MarkingAdded("success")); //event(new \App\Events\MarkingAdded($marking));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
        } else {
            $lastMarking = $this->getLastMarking($request->user_id);
            if ($lastMarking) {

                $lastMarking->clock_out = $dateMarking;
                $lastMarking->contract_id = $contract->id;
                $lastMarking->status = '2';
                $lastMarking->save();
                try {
                    // event(new \App\Events\MarkingAdded("success")); //event(new \App\Events\MarkingAdded($lastMarking));
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
        }
        return $this->getMarkingByUser($request->user_id);
    }

    public function storeFromAdmin(Request $request)
    {

        $agent = new Agent();

        if (!$agent->isWindows()) {
            return response()->json([
                "status" => 400,
                'message' => 'This action cannot be performed from a Mobile device'
            ], 400);
        }


        // //Validate if is a Mobile
        // $isMobile = $this->is_mobile();

        // if($isMobile){
        //     return response()->json([
        //         "status" => 400,
        //         'message' => 'This action cannot be performed from a Mobile device'
        //     ], 400);
        // }

        $contract = Contract::where('user_id', $request->user_id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->first();

        $marking = Marking::create([
            'user_id'     => $request->user_id,
            'contract_id' => $contract->id,
            'clock_in'    => $request->date . ' ' . $request->timein . ':00',
            'clock_out'    => $request->date . ' ' . $request->timeout . ':00',
            'date'    => $request->date,
            'status'    => 2,

        ]);
        try {
            // event(new \App\Events\MarkingAdded("success")); //event(new \App\Events\MarkingAdded($marking));
        } catch (\Throwable $th) {
            //throw $th;
        }

        return new MarkingResource($marking);
    }

    public function getMarkingByUser($id)
    {
        $date = Carbon::parse(Carbon::now())->setTimezone('America/Los_Angeles')->format('Y-m-d');

        $lastMarking  = Marking::select(
            'id',
            DB::raw("(DATE_FORMAT(clock_in,'%Y-%m-%d %H:%i')) as clk_in"),
            DB::raw("(DATE_FORMAT(clock_out,'%Y-%m-%d %H:%i')) as clk_out"),
            'status'

        )
            ->where('user_id', $id)
            ->where('date', $date)
            ->orderBy('clk_in', 'asc')
            ->get();

        return  $lastMarking;
    }

    public function getLastMarking($id)
    {

        $date = Carbon::parse(Carbon::now())->setTimezone('America/Los_Angeles')->format('Y-m-d');

        $lastMarking  = Marking::where('user_id', $id)
            ->where('status', '1')
            ->where('date', $date)
            ->OrderBy('id', 'desc')
            ->first();

        return $lastMarking;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\Response
     */
    public function show(Marking $marking)
    {
        return new MarkingResource($marking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\Response
     */
    public function edit(Marking $marking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMarkingRequest  $request
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMarkingRequest $request, Marking $marking)
    {
        $marking->clock_in = $marking->date . ' ' . $request->timein . ':00';
        if ($request->timeout <> "") {
            $marking->clock_out = $marking->date . ' ' . $request->timeout . ':00';
            $marking->status = 2;
        }
        $marking->reason =  $request->reason;
        $marking->update();
        return new MarkingResource($marking);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marking $marking)
    {
        $marking->delete();

        return response()->json([
            "status" => 200,
            'message' => 'Marking deleted!'
        ], 200);
    }

    public function exportMarkings(Request $request)
    {
        return Excel::download(new MarkingsExport($request->check, $request->from_date, $request->to_date), 'markings.xlsx');
    }

    //Esta función comprueba si la cadena "User-Agent" contiene una de las palabras
    //clave en el array $mobile_agents.
    // si existeedevuelve true que indica que la petición se hizo desde un dispositivo móvil.
    //  De lo contrario, se devuelve false.
    public function is_mobile()
    {
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $mobile_agents = array(
            'iphone', 'ipod', 'ipad', 'android', 'webos', 'blackberry',
            'nokia', 'opera mini', 'windows mobile', 'windows phone',
            'iemobile', 'kindle', 'silk'
        );
        foreach ($mobile_agents as $agent) {
            if (strpos($user_agent, $agent) !== false) {
                return true;
            }
        }
        return false;
    }
}
