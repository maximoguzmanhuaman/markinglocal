<?php

namespace App\Http\Controllers;

use App\Http\Resources\PermissionResource;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PermissionResource::collection(Permission::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreParentTitleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreParentTitleRequest $request)
    {
        $this->authorize('parent_titles.create');
        $parent_title = ParentTitle::create($request->validated());
        return new ParentTitleResource($parent_title);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ParentTitle  $parentTitle
     * @return \Illuminate\Http\Response
     */
    public function show(ParentTitle $parentTitle)
    {
        return new ParentTitleResource($parentTitle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateParentTitleRequest  $request
     * @param  \App\Models\ParentTitle  $parentTitle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateParentTitleRequest $request, ParentTitle $parentTitle)
    {
        $this->authorize('parent_titles.update');
        $parentTitle->update($request->validated());
        return new ParentTitleResource($parentTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ParentTitle  $parentTitle
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentTitle $parentTitle)
    {

        $this->authorize('parent_titles.delete');

        $parentTitle->delete();

        return response()->noContent();
    }
}
