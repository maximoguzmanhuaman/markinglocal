<?php

namespace App\Http\Controllers;

use App\Http\Resources\ContractResource;
use App\Models\Contract;
use App\Models\Marking;
use App\Models\User;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ContractResource::collection(Contract::orderBy('status','desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => ['required', 'exists:users,id'],
            'account_id' => ['required', 'exists:accounts,id'],
            'job_title_id' => ['required', 'exists:job_titles,id'],
            'status' => 'boolean',
            'time_of_entry' => 'required',
            'time_of_out' => 'required',
            'contract_date_start' => 'required',
            'contract_date_end' => 'required'
        ]);

        //Si ya tiene un contrato activo, se actualizan a inactivo y se crea el nuevo con status active
        $seachContract = Contract::where('user_id',$request['user_id'])->where('status',1)->orderBy('id','desc')->first();
        if($seachContract){
            Contract::where("user_id", $request['user_id'])->update(["status" => 0]);
        }

        //Se crea el nuevo Contract
        $newContract = Contract::create([
            'user_id'     => $request['user_id'],
            'account_id'    => $request['account_id'],
            'job_title_id'    => $request['job_title_id'],
            'status' => $request['status'],
            'time_of_entry' => $request['time_of_entry'],
            'time_of_out' => $request['time_of_out'],
            'contract_date_start' => $request['contract_date_start'],
            'contract_date_end' => $request['contract_date_end'],
        ]);
        //Luego de crearse se busca el nuevo Contract y se actualiza el Usuario con el nuevo contract_id
        $updateContractUsers = Contract::where('user_id',$request['user_id'])->where('status',1)->orderBy('id','desc')->first();
        User::where("id", $request['user_id'])->update(
            [
                "contract_id" => $updateContractUsers->id,
                "account_id" => $updateContractUsers->account_id,
                "job_title_id" => $updateContractUsers->job_title_id,
                "time_of_entry" => $updateContractUsers->time_of_entry,
                "time_of_out" => $updateContractUsers->time_of_out,

            ]);
        
        //Se actualizan las marcaciones teniendo en cuenta el Contract Start Date y  Contract End Date
        Marking::where("user_id", $request['user_id'])
                ->whereDate('date', '>=', $request['contract_date_start'])
                ->whereDate('date', '<=', $request['contract_date_end'])
                ->update(["contract_id" => $updateContractUsers->id]);

        return new ContractResource($newContract);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        return new ContractResource($contract);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {

        
    // dd($request);
        $request->validate([
            'user_id' => ['required', 'exists:users,id'],
            'account_id' => ['required', 'exists:accounts,id'],
            'job_title_id' => ['required', 'exists:job_titles,id'],
            'status' => 'boolean',
            'time_of_entry' => 'required',
            'time_of_out' => 'required',
            'contract_date_start' => 'required',
            'contract_date_end' => 'required'
        ]);


        $contract->account_id =  $request['account_id'];
        $contract->job_title_id =  $request['job_title_id'];
        $contract->time_of_entry =  $request['time_of_entry'];
        $contract->time_of_out =  $request['time_of_out'];
        $contract->contract_date_start =  $request['contract_date_start'];
        $contract->contract_date_end =  $request['contract_date_end'];
        $contract->status =  $request['status'];
        // return  $contract ;
        $contract->save();

        if($request['status'] == 1){
            $user = User::where('id',$request['user_id'] )->first();
            $user->account_id  = $request['account_id'];
            $user->job_title_id   = $request['job_title_id'];
            $user->time_of_entry  = $request['time_of_entry'];
            $user->time_of_out  = $request['time_of_out'];
            $user->save();

             //Se actualizan las marcaciones teniendo en cuenta el Contract Start Date y  Contract End Date
            Marking::where("user_id", $request['user_id'])
                    ->whereDate('date', '>=', $request['contract_date_start'])
                    ->whereDate('date', '<=', $request['contract_date_end'])
                    ->update(["contract_id" => $contract->id]);

        }


        return new ContractResource($contract);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
