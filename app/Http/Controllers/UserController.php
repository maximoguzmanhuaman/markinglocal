<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceEdit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Models\Contract;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return UserResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAccountRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('accounts.create');

        $request->validate([
            'name' => 'required',
            'dni' => 'required|string|min:8|max:9|unique:users,dni',
            'account_id' => ['required', 'exists:accounts,id'],
            'job_title_id' => ['required', 'exists:job_titles,id'],
            'document_type_id' => ['required', 'exists:document_types,id'],
            // 'role' => ['array'],
            'role.*.id' => [
                'integer',
                'exists:roles,id'
            ],
            'status' => 'boolean',
            // 'summer_schedule' => 'boolean',
            'date_hired' => 'required',
            'time_of_entry' => 'required',
            'time_of_out' => 'required',
            // 'email' => 'required|unique:users,email',
            'password' => 'string'
        ]);

        $user = User::create([
            'name'     => $request['name'],
            // 'email'    => $request['email'],
            'dni'    => $request['dni'],
            'account_id'    => $request['account_id'],
            'job_title_id'    => $request['job_title_id'],
            'document_type_id'    => $request['document_type_id'],
            'status' => $request['status'],
            // 'summer_schedule' =>  $request['summer_schedule'],
            'date_hired'    => $request['date_hired'],
            'time_of_entry' => $request['time_of_entry'],
            'time_of_out' => $request['time_of_out'],
            'password' => Hash::make($request['password']),

        ]);
        $user->roles()->sync($request->input('role', []));

        //Para no automatcamente cree un contract y actualice el users contract_id falta
        // $userCreated = User::where('dni',$request['dni'])->where('status',1)->first();

        // //Se crea el nuevo Contract
        // $newContract = Contract::create([
        //     'user_id'     => $userCreated->id,
        //     'account_id'    => $userCreated->account_id,
        //     'job_title_id'    => $userCreated->job_title_id,
        //     'status' => 1,
        //     'time_of_entry' => $userCreated->time_of_entry,
        //     'time_of_out' => $userCreated->time_of_out
        // ]);
        // exit;

        // $user = User::create($request->validated());

        // $user =  User::create([
        //     'name'     => $request['name'],
        //     'email'    => $request['email'],
        //     'dni'    => $request['dni'],
        //     'account_id'    => $request['account_id'],
        //     'date_hired'    => $request['date_hired'],
        //     'schedule'    => $request['schedule'],
        //     'password' => Hash::make($request['password']),
        // ]);


        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $parentTitle
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResourceEdit($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // // $this->authorize('users.update');

        $request->validate([
            'name' => 'required',
            // 'dni' => 'required|string|min:8|max:9',
            'dni' => 'required|unique:users,dni,' . $user->id,
            'account_id' => ['required', 'exists:accounts,id'],
            'job_title_id' => ['required', 'exists:job_titles,id'],
            'document_type_id' => ['required', 'exists:document_types,id'],
            'role.*.id' => [
                'integer',
                'exists:roles,id'
            ],
            'status' => 'boolean',
            // 'summer_schedule' => 'boolean',
            'date_hired' => 'required',
            'time_of_entry' => 'required',
            'time_of_out' => 'required',
            // 'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => ''
        ]);




        $user->name =  $request['name'];
        // $user->email =  $request['email'];
        $user->dni =  $request['dni'];
        $user->account_id =  $request['account_id'];
        $user->job_title_id =  $request['job_title_id'];
        $user->document_type_id =  $request['document_type_id'];
        $user->status = $request['status'];
        // $user->summer_schedule = $request['summer_schedule'];
        $user->date_hired =  $request['date_hired'];
        $user->time_of_entry = $request['time_of_entry'];
        $user->time_of_out = $request['time_of_out'];
        if (isset($request['password']) && strlen($request['password']) >= 8) {
            $user->password =  Hash::make($request['password']);
        }

        $user->save();
        $user->roles()->sync($request->input('role', []));

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        // $this->authorize('users.delete');

        $user->delete();

        // return response()->noContent();
        return response()->json([
            "status" => 200,
            'message' => 'Record deleted'
        ], 200);
    }

    public function exportUsers()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
