<?php

namespace App\Http\Controllers;

use App\Models\JobTitle;
use App\Http\Requests\StoreJobTitleRequest;
use App\Http\Requests\UpdateJobTitleRequest;
use App\Http\Resources\JobTitleResource;
use App\Models\User;

class JobTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return JobTitleResource::collection(JobTitle::orderBy('name')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJobTitleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobTitleRequest $request)
    {
        $jobTitle = JobTitle::create($request->validated());
        return new JobTitleResource($jobTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobTitle  $jobTitle
     * @return \Illuminate\Http\Response
     */
    public function show(JobTitle $jobTitle)
    {
        return new JobTitleResource($jobTitle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJobTitleRequest  $request
     * @param  \App\Models\JobTitle  $jobTitle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJobTitleRequest $request, JobTitle $jobTitle)
    {
        $jobTitle->update($request->validated());
        return new JobTitleResource($jobTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobTitle  $jobTitle
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobTitle $jobTitle)
    {
        $usr = User::where('job_title_id', $jobTitle->id)->count();
        if ($usr > 0) {
            return response()->json([
                "status" => 403,
                'message' => 'Error, you cannot delete a Job Position that is assigned to an User'
            ], 200);
        } else {
            $jobTitle->delete();
            return response()->json([
                "status" => 200,
                'message' => 'Job Position delete!'
            ], 200);
        }
    }
}
