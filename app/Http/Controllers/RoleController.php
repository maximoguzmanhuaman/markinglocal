<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return RoleResource::collection(Role::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreParentTitleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        $role = Role::create($request->validated());
        // $role->permissions()->sync($request->input('permissions', []));

        return new RoleResource($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoleRequest  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $role->update($request->validated());
        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ParentTitle  $parentTitle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {

        $userCount = User::
        whereHas(
            'roles', function($q) use ($role){
                $q->where('id',$role->id );
            }
        )->count();

        if ($userCount > 0) {
            return response()->json([
                "status" => 403,
                'message' => 'Error, you cannot delete a Role that is assigned to an User'
            ], 200);
        } else {
            $role->delete();
            return response()->json([
                "status" => 200,
                'message' => 'Role deleted!'
            ], 200);
        }
    }
}
