<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'account_id', 
        'job_title_id', 
        'time_of_entry', 
        'time_of_out',
        'contract_date_start',
        'contract_date_end',
        'status'
    ];

    protected $casts = [
        'time_of_entry' =>  'datetime',
        'time_of_out' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function jobtitle()
    {
        return $this->belongsTo(JobTitle::class, 'job_title_id');
    }
}
