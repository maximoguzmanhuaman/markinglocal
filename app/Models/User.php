<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'dni',
        'account_id',
        'job_title_id',
        'document_type_id',
        'role',
        'status',
        'summer_schedule',
        'date_hired',
        'time_of_entry',
        'time_of_out',
        // 'email',
        'password',
        'contract_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'time_of_entry' =>  'datetime',
        'time_of_out' => 'datetime',
        'status' => 'boolean',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function jobtitle()
    {
        return $this->belongsTo(JobTitle::class, 'job_title_id');
    }
    public function documenttype()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id');
    }
}
