<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marking extends Model
{
    use HasFactory, SoftDeletes;

    protected $casts = [
        'clock_in' =>  'datetime',
        'clock_out' =>  'datetime',
    ];

    // protected $fillable = ['user_id', 'entry_1', 'out_1', 'entry_2', 'out_2', 'entry_3', 'out_3', 'entry_4', 'out_4', 'clock_in', 'clock_out', 'status', 'date', 'reason','contract_id'];
   
    protected $fillable = ['user_id','contract_id','date','clock_in', 'clock_out', 'status', 'reason',];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
