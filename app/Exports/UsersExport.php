<?php

namespace App\Exports;

use App\Http\Resources\MarkingResource;
use App\Http\Resources\UserResource;
use App\Models\Marking;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class UsersExport implements FromView, ShouldAutoSize, WithDrawings
{

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        // $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('logo.jpg'));
        $drawing->setHeight(65);
        $drawing->setCoordinates('A1');


        return [$drawing];
    }


    public function view(): View
    {

        $data = Marking::with(['user' => function ($query) {
            $query->with('account');
        },])->get();


        return view('exports.users.users', [
            'users' => User::all()
        ]);
    }
}
