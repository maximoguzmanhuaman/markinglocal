<?php

namespace App\Exports;

use App\Http\Resources\MarkingResource;
use App\Models\Marking;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Carbon\Carbon;
use DB;
use IntlCalendar;
use IntlDateFormatter;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;

class MarkingsExport implements FromView, ShouldAutoSize, WithDrawings, WithEvents
{
    use RegistersEventListeners;

    public $check, $from_date, $to_date; 

    public function __construct($check, $from_date, $to_date)
    {
        $this->check = $check;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }


    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        // $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('logo.jpg'));
        $drawing->setHeight(65);
        $drawing->setCoordinates('A1');


        return [$drawing];
    }


    public function view(): View
    {
        $data = DB::select('call export_report_markings(?,?);', array($this->from_date, $this->to_date));

        if ($this->check == "default") {
            foreach ($data as $element) {
                $date = Carbon::parse($element->FECHA, 'America/Los_Angeles')->startOfDay()->format('Y-m-d H:i:s');
                $dateFormated = IntlCalendar::fromDateTime($date);
                $element->FECHA = ucwords(IntlDateFormatter::formatObject($dateFormated, "EEEE ',' dd/MM/y", 'es_ES'));
                $element->REASON = $element->REASON;
                $element->update_1 = $element->update_1;
                $element->update_2 = $element->update_2;
                $element->update_3 = $element->update_3;
                $element->update_4 = $element->update_4;
                $element->JORNADA = $element->time_of_entry != '--' ? Carbon::parse($element->time_of_entry, 'America/Los_Angeles')->format('H:i') . ' - ' . Carbon::parse($element->time_of_out, 'America/Los_Angeles')->format('H:i') : '-';
                $element->in_1 =  ($element->in_1 <> "") ? Carbon::parse($element->in_1, 'America/Los_Angeles')->format('H:i') : "";
                $element->in_2 = ($element->in_2 <> "") ? Carbon::parse($element->in_2, 'America/Los_Angeles')->format('H:i') : "";
                $element->in_3 = ($element->in_3 <> "") ? Carbon::parse($element->in_3, 'America/Los_Angeles')->format('H:i') : "";
                $element->in_4 = ($element->in_4 <> "") ? Carbon::parse($element->in_4, 'America/Los_Angeles')->format('H:i') : "";
                $element->out_1 = ($element->out_1 <> "") ? Carbon::parse($element->out_1, 'America/Los_Angeles')->format('H:i') : "";
                $element->out_2 = ($element->out_2 <> "") ? Carbon::parse($element->out_2, 'America/Los_Angeles')->format('H:i') : "";
                $element->out_3 = ($element->out_3 <> "") ? Carbon::parse($element->out_3, 'America/Los_Angeles')->format('H:i') : "";
                $element->out_4 = ($element->out_4 <> "") ? Carbon::parse($element->out_4, 'America/Los_Angeles')->format('H:i') : "";
            }
        } else {
            foreach ($data as $element) {
                $date = Carbon::parse($element->FECHA, 'America/Los_Angeles')->timezone('America/Lima')->startOfDay()->format('Y-m-d H:i:s');
                $dateFormated = IntlCalendar::fromDateTime($date);
                $element->FECHA = ucwords(IntlDateFormatter::formatObject($dateFormated, "EEEE ',' dd/MM/y", 'es_ES'));

                $element->JORNADA = $element->time_of_entry != '--' ? Carbon::parse($element->time_of_entry, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') . ' - ' . Carbon::parse($element->time_of_out, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i'):  '-';
                $element->in_1 = ($element->in_1 <> "") ? Carbon::parse($element->in_1, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->in_2 = ($element->in_2 <> "") ? Carbon::parse($element->in_2, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->in_3 = ($element->in_3 <> "") ? Carbon::parse($element->in_3, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->in_4 = ($element->in_4 <> "") ? Carbon::parse($element->in_4, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->out_1 = ($element->out_1 <> "") ? Carbon::parse($element->out_1, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->out_2 = ($element->out_2 <> "") ? Carbon::parse($element->out_2, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->out_3 = ($element->out_3 <> "") ? Carbon::parse($element->out_3, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
                $element->out_4 = ($element->out_4 <> "") ? Carbon::parse($element->out_4, 'America/Los_Angeles')->timezone('America/Lima')->format('H:i') : "";
            }
        }

        return view('exports.markings.markings', [
            "showReason" => $this->check,
            'markings' => $data,
            'from' => date("F j, Y", strtotime($this->from_date)),
            'to' => date("F j, Y", strtotime($this->to_date))
        ]);
    }
}
