<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccountSeeder::class);
        $this->call(JobTitleSeeder::class);
        $this->call(DocumentTypeSeeder::class);
        \App\Models\User::factory(20)->create();
        \App\Models\ParentTitle::factory(5)->create();
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);

        // \App\Models\Marking::factory(50)->create();

    }
}
