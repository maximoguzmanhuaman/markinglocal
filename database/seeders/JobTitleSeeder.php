<?php

namespace Database\Seeders;

use App\Models\JobTitle;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobTitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobTitle::create(['name' => 'Líder de Contabilidad']);
        JobTitle::create(['name' => 'Supervisora de Operaciones']);
        JobTitle::create(['name' => 'Conserge']);
        JobTitle::create(['name' => 'Entrenadora']);
        JobTitle::create(['name' => 'Reclutadora']);
        JobTitle::create(['name' => 'Analista Funcional Jr.']);
        JobTitle::create(['name' => 'Progamador Web Jr.']);
        JobTitle::create(['name' => 'Programador Web']);
        JobTitle::create(['name' => 'Diseñador Web']);
        JobTitle::create(['name' => 'Programador Senior']);
        JobTitle::create(['name' => 'Diseñador Web Jr.']);
        JobTitle::create(['name' => 'Especialista de IT']);
        JobTitle::create(['name' => 'CSR']);
        JobTitle::create(['name' => 'Ajustadora']);

        // DB::table('job_titles')->insert([
        //     [
        //         "name" => 'Líder de Contabilidad',
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        //     [
        //         "name" => 'Diseñador Web Jr.',
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        //     ],
        // ]);
    }
}
