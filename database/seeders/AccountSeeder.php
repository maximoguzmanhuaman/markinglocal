<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create(['name' => 'Ops Lead']);
        Account::create(['name' => 'Aspire - Claims']);
        Account::create(['name' => 'Aspire']);
        Account::create(['name' => 'Aspire - MKTG']);
        Account::create(['name' => 'InsZone']);
        Account::create(['name' => 'Fiesta 1.0']);
        Account::create(['name' => 'Staff Boom']);
    }
}
