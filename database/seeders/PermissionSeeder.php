<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'parent_titles.create']);
        Permission::create(['name' => 'parent_titles.update']);
        Permission::create(['name' => 'parent_titles.delete']); 

    }
}
