<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreProcedureReportExportMarkings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        DROP PROCEDURE IF EXISTS export_report_markings_default; CREATE PROCEDURE `export_report_markings_default`(
            IN from_date date, 
            IN to_date date
        )
            BEGIN
                SELECT 
                acc.name as 'ACCOUNT',
                u.name as 'NOMBRE_COMPLETO',
                u.dni as 'DOCUMENTO',
                concat( TIME_FORMAT(u.time_of_entry,'%H:%i') , ' - ' , TIME_FORMAT(u.time_of_out,'%H:%i') ) as 'JORNADA', 
                (m.user_id) as 'USER_ID',
                (m.date) as 'FECHA',
                (SELECT DATE_FORMAT(clock_in, '%H:%i') FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 0,1) AS 'in_1',
                (SELECT DATE_FORMAT(clock_out, '%H:%i') FROM markings where  user_id = m.user_id and date=m.date order by 1 limit 0,1) AS 'out_1',
                (SELECT DATE_FORMAT(clock_in, '%H:%i') FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 1,1) AS 'in_2',
                (SELECT DATE_FORMAT(clock_out, '%H:%i') FROM markings where  user_id = m.user_id and date=m.date order by 1 limit 1,1) AS 'out_2',
                (SELECT DATE_FORMAT(clock_in, '%H:%i') FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 2,1) AS 'in_3',
                (SELECT DATE_FORMAT(clock_out, '%H:%i') FROM markings where  user_id = m.user_id and date=m.date order by 1 limit 2,1) AS 'out_3',
                (SELECT DATE_FORMAT(clock_in, '%H:%i') FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 3,1) AS 'in_4',
                (SELECT DATE_FORMAT(clock_out, '%H:%i') FROM markings where  user_id = m.user_id and date=m.date order by 1 limit 3,1) AS 'out_4',
                ( 
                    SELECT TIMESTAMPDIFF(
                                        MINUTE,(SELECT clock_in FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 0,1),
                                        (SELECT clock_out FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 0,1)
                                
                            ) + 

                            TIMESTAMPDIFF(
                                        MINUTE,(SELECT clock_in FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 1,1),
                                        (SELECT clock_out FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 1,1)
                                
                            ) +

                            TIMESTAMPDIFF(
                                        MINUTE,(SELECT clock_in FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 2,1),
                                        (SELECT clock_out FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 2,1)
                                
                            ) +

                            TIMESTAMPDIFF(
                                        MINUTE,(SELECT clock_in FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 3,1),
                                        (SELECT clock_out FROM markings where  user_id = m.user_id and date= m.date order by 1 limit 3,1)
                                
                            ) 
                
            
                ) as TotalTime,

				(SELECT IF(TotalTime - 450 > 0, TotalTime - 450, 0)) as Extras

                FROM 
                    markings AS m
                    INNER JOIN users u ON u.id = m.user_id
                    INNER JOIN accounts acc ON acc.id = u.account_id
                    WHERE m.date >= from_date and m.date <= to_date
                GROUP BY m.date,m.user_id;
            END
        ";
        \DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
